#include "torque_qp/controller/controller.h"

using namespace std;
using namespace pinocchio;

namespace Controller
{
bool Controller::init(ros::NodeHandle &node_handle, Eigen::VectorXd q_init, Eigen::VectorXd qdot_init)
{
    ROS_INFO("Using torque_qp controller");
    bool initialized;
    //--------------------------------------
    // INITIALIZE PUBLISHERS
    //--------------------------------------
    if (!initPublishers(node_handle))
        return false;

    //--------------------------------------
    // LOAD ROBOT
    //--------------------------------------
    if (!loadRobot(node_handle))
        return false;

    //--------------------------------------
    // LOAD PARAMETERS
    //--------------------------------------
    if (!loadParameters())
        return false;

    //--------------------------------------
    // QPOASES
    //--------------------------------------
    qp = qp_solver.configureQP(number_of_variables, number_of_constraints);

    //--------------------------------------
    // INITIALIZE RBOT STATE
    //--------------------------------------
    // First calls the forwardKinematics on the model, then computes the placement of each frame.
    pinocchio::framesForwardKinematics(*reduced_model,*data,q_init);
    Eigen::Affine3d oMtip;
    oMtip = data->oMf[reduced_model->getFrameId(tip_link)].toHomogeneousMatrix();

    //--------------------------------------
    // BUILD TRAJECTORY
    //--------------------------------------
    std::string panda_traj_path = ros::package::getPath("panda_traj");
    std::string trajectoryfile = panda_traj_path+"/trajectories/go_to_point.csv";
    trajectory.LoadFromPath(trajectoryfile);
    trajectory.Build(oMtip, false);
   
    ROS_DEBUG_STREAM(" Trajectory computed ");
    ROS_INFO("Controller configuration done");
    return true;
}

Eigen::VectorXd Controller::update(Eigen::VectorXd q, Eigen::VectorXd qdot, Eigen::VectorXd tau_J_d, const ros::Duration &period)
{
    double time_dt = period.toSec();
    // Update the model
    // First calls the forwardKinematics on the model, then computes the placement of each frame.
    pinocchio::forwardKinematics(*model,*data,q,qdot,0.0*qdot);
    pinocchio::updateFramePlacements(*reduced_model,*data);
    pinocchio::computeMinverse(*reduced_model,*data,q);
    data->Minv.triangularView<Eigen::StrictlyLower>() = data->Minv.transpose().triangularView<Eigen::StrictlyLower>();
    pinocchio::computeGeneralizedGravity(*reduced_model,*data,q);
    pinocchio::computeCoriolisMatrix(*reduced_model,*data,q,qdot);
    pinocchio::computeJointJacobians(*reduced_model,*data,q);
    pinocchio::framesForwardKinematics(*reduced_model,*data,q);
    pinocchio::getFrameJacobian(*reduced_model, *data, reduced_model->getFrameId(tip_link), pinocchio::ReferenceFrame::LOCAL, J);
    pinocchio::computeJointJacobiansTimeVariation(*reduced_model,*data,q,qdot);
    Jdot_qdot = getFrameClassicalAcceleration(*model, *data, model->getFrameId(tip_link),pinocchio::ReferenceFrame::LOCAL_WORLD_ALIGNED).toVector();
    // Current velocity expressed in the LOCAL frame
    xd = pinocchio::getFrameVelocity(*reduced_model, *data, reduced_model->getFrameId(tip_link),pinocchio::ReferenceFrame::LOCAL);
    
    //Update the trajectory
    ROS_DEBUG_STREAM("Updating trajectory");
    trajectory.updateTrajectory(traj_properties, time_dt);
    pinocchio::SE3 oMdes(trajectory.Pos().matrix());
    
    // Compute error
    const pinocchio::SE3 tipMdes = data->oMf[reduced_model->getFrameId(tip_link)].actInv(oMdes);
    err = pinocchio::log6(tipMdes).toVector();
    ROS_DEBUG_STREAM("Computing xdd_des");
    xdd_des = p_gains.cwiseProduct(err) - d_gains.cwiseProduct(xd.toVector());

    // Formulate QP problem such that
    // joint_torque_out = argmin 1/2 tau^T H tau + tau^T g_
    //                       s.t     lbA < A tau < ubA
    //                                   lb < tau < ub 
    nonLinearTerms = data->Minv * (data->C* qdot + data->g);
    
    qp.hessian  = 2.0 * regularisation_weight *  data->Minv;
    qp.gradient = 2.0 * regularisation_weight *  data->Minv * k_reg.asDiagonal()  * qdot;
    
    a.noalias() = J * data->Minv;
    b.noalias() =  Jdot_qdot - J*data->Minv * data->C* qdot - xdd_des;

    qp.hessian  +=  2.0 * a.transpose() *  a;
    qp.gradient +=  2.0 * a.transpose() * b;
    

    qp.ub = ( reduced_model->effortLimit - data->g).cwiseMin( dtorque_max * 0.001 + tau_J_d);
    qp.lb = (-reduced_model->effortLimit - data->g).cwiseMax(-dtorque_max * 0.001 + tau_J_d);

    
    double horizon_dt = 15 * time_dt;
    qp.a_constraints.block(0, 0, dof, dof) = data->Minv;
    qp.lb_a.block(0, 0, dof, 1) =
        ((-reduced_model->velocityLimit - qdot) / horizon_dt + nonLinearTerms)
            .cwiseMax(2 * (reduced_model->lowerPositionLimit - q - qdot * horizon_dt) / (horizon_dt * horizon_dt) + nonLinearTerms);

    qp.ub_a.block(0, 0, dof, 1) =
        ((reduced_model->velocityLimit - qdot) / horizon_dt + nonLinearTerms)
            .cwiseMin(2 * (reduced_model->upperPositionLimit - q - qdot * horizon_dt) / (horizon_dt * horizon_dt) + nonLinearTerms);
            
    // Publish some messages
    doPublishing();
    ROS_DEBUG_STREAM("Solving qp");
    return qp_solver.SolveQP(qp);
}

void Controller::buildTrajectory(Eigen::Affine3d X_curr)
{
    trajectory.Build(X_curr, false);
    publishTrajectory();
}

bool Controller::loadParameters()
{
    ROS_INFO_STREAM("------------- Loading parameters -------------");
    p_gains.resize(6);
    getRosParam("/panda/torque_qp/p_gains", p_gains,true);
    d_gains.resize(6);
    getRosParam("/panda/torque_qp/d_gains", d_gains,true);
    k_reg.resize(dof);
    getRosParam("/panda/torque_qp/k_reg", k_reg,true);
    dtorque_max.resize(dof);
    getRosParam("/panda/torque_qp/dtorque_max", dtorque_max,true);
    getRosParam("/panda/torque_qp/regularisation_weight", regularisation_weight,true);
    getRosParam("/panda/torque_qp/tip_link",tip_link,true);
    ros::param::get("/panda/torque_qp/activate_main_task", activate_main_task);
    ROS_INFO_STREAM("------------- Parameters Loaded -------------");

    return true;
}

bool Controller::loadRobot(ros::NodeHandle& node_handle)
{
    updateUI_service = node_handle.advertiseService("updateUI", &Controller::updateUI, this);
    updateTraj_service = node_handle.advertiseService("updateTrajectory", &Controller::updateTrajectory, this); 
    trajProgress_service = node_handle.advertiseService("TrajectoryProgress", &Controller::TrajectoryProgress, this); 

    ROS_INFO_STREAM("------------- Getting Model -------------");
    ROS_DEBUG_STREAM("Initializing model");
    // get robot descritpion
    std::string urdf_param;
    getRosParam("/panda/robot_description", urdf_param);
    // Load the urdf model
    model.reset(new pinocchio::Model);
    reduced_model.reset(new pinocchio::Model);
    pinocchio::urdf::buildModelFromXML(urdf_param,*model,false);
    std::vector<std::string> list_of_joints_to_keep_unlocked_by_name;
    ros::param::get("/panda/torque_qp/joint_names", list_of_joints_to_keep_unlocked_by_name);

    std::vector<JointIndex> list_of_joints_to_keep_unlocked_by_id;
    for(std::vector<std::string>::const_iterator it = list_of_joints_to_keep_unlocked_by_name.begin();
        it != list_of_joints_to_keep_unlocked_by_name.end(); ++it)
    {
        const std::string & joint_name = *it;
        if(model->existJointName(joint_name))
        list_of_joints_to_keep_unlocked_by_id.push_back(model->getJointId(joint_name));
        else
        std::cout << "joint: " << joint_name << " does not belong to the model";
    }
    
    // Transform the list into a list of joints to lock
    std::vector<JointIndex> list_of_joints_to_lock_by_id;
    for(JointIndex joint_id = 1; joint_id < model->joints.size(); ++joint_id)
    {
        const std::string joint_name = model->names[joint_id];
        if(is_in_vector(list_of_joints_to_keep_unlocked_by_name,joint_name))
        continue;
        else
        {
        list_of_joints_to_lock_by_id.push_back(joint_id);
        }
    }
    
    // Build the reduced model from the list of lock joints
    Eigen::VectorXd q_rand = randomConfiguration(*model);
    pinocchio::buildReducedModel(*model,list_of_joints_to_lock_by_id,q_rand,*reduced_model);
    data.reset(new pinocchio::Data(*reduced_model));

    dof = reduced_model->nv;

    //--------------------------------------
    // INITIALIZE VARIABLES
    //--------------------------------------    

    Jdot = Eigen::MatrixXd::Zero(6,dof);
    J = Eigen::MatrixXd::Zero(6,dof);
    nonLinearTerms.resize(dof);
    a.resize(6,dof);


    number_of_variables = dof;
    number_of_constraints = dof;
    
    return true;
}

// Ros service to interact with the code
bool Controller::updateUI(torque_qp::UI::Request &req, torque_qp::UI::Response &resp)
{
    traj_properties.play_traj_ = req.play_traj;
    if (req.publish_traj)
        publishTrajectory();
    if (req.build_traj)
    {
        Eigen::Affine3d X_curr(data->oMf[reduced_model->getFrameId(tip_link)].toHomogeneousMatrix()); 
        buildTrajectory(X_curr);
    }

    if (req.exit_)
    {
        ros::shutdown();
        exit(0);
    } 
    resp.result = true;

    return true;
}

// Ros service to update the trajectory
bool Controller::updateTrajectory(panda_traj::UpdateTrajectory::Request& req, panda_traj::UpdateTrajectory::Response& resp){
  if (req.is_path)
  {
    trajectory.LoadFromPath(req.csv);
  }
  else
  {
    trajectory.LoadFromString(req.csv);
  }
  Eigen::Affine3d X_curr(data->oMf[reduced_model->getFrameId(tip_link)].toHomogeneousMatrix()); 
  trajectory.Build(X_curr, req.verbose);
  publishTrajectory();
  std::cout << "Received waypoint computing traj and publishing" << std::endl;

  return true;
}

bool Controller::TrajectoryProgress(panda_traj::TrajectoryProgress::Request& req, panda_traj::TrajectoryProgress::Response& resp){
  resp.progress = trajectory.getTimeProgress();
  return true;
}


void Controller::publishTrajectory()
    {
        panda_traj::PublishTraj publish_traj_;
        publish_traj_ = trajectory.publishTrajectory();

        nav_msgs::Path path_ros;
        path_ros.poses = publish_traj_.path_ros_.poses;
        path_ros.header.frame_id = "world";
        path_ros.header.stamp = ros::Time::now();
        geometry_msgs::PoseArray pose_array;
        pose_array.poses = publish_traj_.pose_array_.poses;
        pose_array.header.frame_id = "world";
        pose_array.header.stamp = ros::Time::now();

        if (pose_array_publisher.trylock())
        {
            pose_array_publisher.msg_.header.stamp = ros::Time::now();
            pose_array_publisher.msg_.header.frame_id = "world";
            pose_array_publisher.msg_ = pose_array;
            pose_array_publisher.unlockAndPublish();
        }
        if (path_publisher.trylock())
        {
            path_publisher.msg_.header.stamp = ros::Time::now();
            path_publisher.msg_.header.frame_id = "world";
            path_publisher.msg_ = path_ros;
            path_publisher.unlockAndPublish();
        }
        ROS_INFO_STREAM(" Trajectory published ");
    }

    bool Controller::initPublishers(ros::NodeHandle &node_handle)
    {
        // Realtime safe publishers
        pose_array_publisher.init(node_handle, "Pose_array", 1);
        path_publisher.init(node_handle, "Ros_Path", 1);
        pose_curr_publisher.init(node_handle, "X_curr", 1);
        pose_des_publisher.init(node_handle, "X_traj", 1);
        panda_rundata_publisher.init(node_handle, "panda_rundata", 1);
        
        return true;
    }


    void Controller::doPublishing()
    {
        // Publishing
        Eigen::Affine3d xcurr,xdes;
        xcurr =  data->oMf[reduced_model->getFrameId(tip_link)].toHomogeneousMatrix();
        tf::poseEigenToMsg(xcurr,X_curr_msg);
        
        pinocchio::SE3 oMdes(trajectory.Pos().matrix());
        xdes=oMdes.toHomogeneousMatrix();

        tf::poseEigenToMsg(xdes,X_traj_msg);
        tf::twistEigenToMsg(err, X_err_msg);
        const ros::Time rosnow = ros::Time::now();

        if (panda_rundata_publisher.trylock())
        {
            panda_rundata_publisher.msg_.header.stamp = rosnow;
            panda_rundata_publisher.msg_.header.frame_id = "world";
            panda_rundata_publisher.msg_.X_err = X_err_msg;
            panda_rundata_publisher.msg_.play_traj_ = traj_properties.play_traj_;
            panda_rundata_publisher.msg_.tune_gains_ = traj_properties.gain_tunning_ ;
            panda_rundata_publisher.unlockAndPublish();
        }

        if (pose_curr_publisher.trylock())
        {
            pose_curr_publisher.msg_.header.stamp = rosnow;
            pose_curr_publisher.msg_.header.frame_id = "world";
            pose_curr_publisher.msg_.pose = X_curr_msg;
            pose_curr_publisher.unlockAndPublish();
        }

        if (pose_des_publisher.trylock())
        {
            pose_des_publisher.msg_.header.stamp = rosnow;
            pose_des_publisher.msg_.header.frame_id = "world";
            pose_des_publisher.msg_.pose = X_traj_msg;
            pose_des_publisher.unlockAndPublish();
        }
    }

} // namespace Controller
